#We Moved
We moved over to GitHub.com (for various reasons). You can find use, with the same codebase, issue tracker, pull requests and more at [https://github.com/retrospect-addon/plugin.video.retrospect](https://github.com/retrospect-addon/plugin.video.retrospect).

The BitBucket repository will remain here for the time being, but will not be further maintained.

The Team